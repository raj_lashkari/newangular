import { Component, OnInit } from '@angular/core';
import { FormControl , FormGroup,  Validators } from '@angular/forms';
import {RegisterService} from '../Service/register.service'

 

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {


  registerForm = new FormGroup({
    firstname:new FormControl('',Validators.required),
    lastname:new FormControl('',Validators.required),
    email:new FormControl('',Validators.required),
    gender:new FormControl('',Validators.required),
    password:new FormControl('',Validators.required),
    confirm_password:new FormControl('',Validators.required),

})
get firstname(){return this.registerForm.get('firstname')}
get lastname(){return this.registerForm.get('lastname')}
get email(){return this.registerForm.get('email')}
get gender(){return this.registerForm.get('gender')}
get password(){return this.registerForm.get('password')}
get confirm_password(){return this.registerForm.get('confirm_password')}

data:any;
msg:any='';

  
constructor(private userdata: RegisterService) {
 }
  conPass = ""
  ngOnInit(): void {

  }

  getValues(val:any){

    this.userdata.registerUser(val).subscribe((result)=>{
      console.log(result);
      this.msg = result
      console.log("regitration"+this.msg)
    
    })
     
  }

}

