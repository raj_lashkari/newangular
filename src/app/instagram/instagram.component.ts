import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-instagram',
  templateUrl: './instagram.component.html',
  styleUrls: ['./instagram.component.css']
})
export class InstagramComponent implements OnInit {
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay:true,  
    autoplaySpeed: 5000,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 8  
      },
      400: {
        items:8 
      },
      740: {
        items: 8  
      },
      940: {
        items:8 
      },

    },
    nav: true
  }
  constructor() { }

  ngOnInit(): void {
  }

}
