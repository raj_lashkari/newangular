import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
import { GalleryService } from 'src/app/Service/gallery.service';
import {ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-gallery-images',
  templateUrl: './gallery-images.component.html',
  styleUrls: ['./gallery-images.component.css'],
  encapsulation:ViewEncapsulation.None
})
export class GalleryImagesComponent implements OnInit {
  data: any;
  id: string | null;

  constructor(private route:ActivatedRoute,private galleryData:GalleryService) { 
    this.id = this.route.snapshot.paramMap.get('id');

    this.galleryData.galleryImages(this.id).subscribe((result)=>{

      this.data = result
    })
  }

  ngOnInit(): void {
  }
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: true,
    autoplay:true,  
    autoplaySpeed: 5000,
    navSpeed: 700,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items:1
      },
      740: {
        items: 1
      },
      940: {
        items:1
      }
    },
    nav: true
  }



}
