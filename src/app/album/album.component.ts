import { Component, OnInit,  ViewEncapsulation } from '@angular/core';
import { GalleryService } from '../Service/gallery.service';



@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css'],
  encapsulation : ViewEncapsulation.None,
})
export class AlbumComponent implements OnInit {

  data: any;
  dragData: any;

  constructor(private galleryData:GalleryService) { }

  ngOnInit(): void {
    this.galleryData.getAlbum().subscribe((result)=>{
      this.data = result
    })

  }
  

   ngOnChanges(){


   }

   listOrderChanged(value:any){
     console.log(value)
     this.galleryData.dragDrop(value).subscribe((result)=>{
      console.log(result);
    })
   }
  
}


