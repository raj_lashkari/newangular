import { Component, OnInit } from '@angular/core';
import {BlogService} from '../../Service/blog.service'
@Component({
  selector: 'app-blog-table',
  templateUrl: './blog-table.component.html',
  styleUrls: ['./blog-table.component.css']
})
export class BlogTableComponent implements OnInit {
  data: any;

  constructor(private blogData:BlogService) { }

  ngOnInit(): void {
    this.blogData.getData().subscribe((result)=>{
      this.data = result
    })
  }
  deleteData(id:any){
   this.blogData.blodDelete(id).subscribe((result)=>{
     console.log(result)
   })
  }
}
