import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BlogService } from 'src/app/Service/blog.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-blog-update',
  templateUrl: './blog-update.component.html',
  styleUrls: ['./blog-update.component.css']
})
export class BlogUpdateComponent implements OnInit {
  imageurls: any;
  arr:any[]=[];
  data:any;
  id: any;
  constructor(private route:ActivatedRoute , private blogData:BlogService , private router:Router) {
    this.id =  this.route.snapshot.paramMap.get('id')
   }

  onSelectFile(event:any) {
    if (event.target.files) {
      for (let i = 0; i < File.length + i; i++) {
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.imageurls = (<FileReader>event.target).result;
        };
        this.arr.push(event.target.files[i].name);
      }
    }
  }

  getValues(value:any){
    console.log(value);
    this.blogData.blodUpdate(this.id,value).subscribe((result)=>{
      console.log(result)
    })
    this.router.navigate(['/admin/blogTable'])
  }
  ngOnInit(): void {
    this.blogData.getDataId(this.id).subscribe((result)=>{
      this.data=result;
    })
  }

}
