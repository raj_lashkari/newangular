import { Component, OnInit } from '@angular/core';
import { BlogService } from 'src/app/Service/blog.service';
@Component({
  selector: 'app-blog-add',
  templateUrl: './blog-add.component.html',
  styleUrls: ['./blog-add.component.css']
})
export class BlogAddComponent implements OnInit {
  imageurls: any;
  imageName: any;


  constructor(private blogData:BlogService) { }

  ngOnInit(): void {
  }
  onSelectFile(event:any) {
    if (event.target.files) {
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.imageurls = (<FileReader>event.target).result;
        };
        this.imageName= event.target.files[0].name;
             
    
    }
  }
  getValues(value:any){
    console.log(value)
    value.thumbnail = this.imageName
    this.blogData.addblog(value).subscribe((result)=>{
      console.log(result)
    })
  }
}
