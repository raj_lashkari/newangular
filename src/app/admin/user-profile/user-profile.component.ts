import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/Service/user.service';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  data: any;

  constructor(private userData:UserService) {
    this.userData.getData().subscribe((result)=>{
      this.data = result;
    })
  
   }
   deleteData(id:any){
     console.log(id)
     this.userData.deleteUser(id).subscribe((result)=>{
       console.log(result);
     })

   }

  ngOnInit(): void {


  }
  

}
