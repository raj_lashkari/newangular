import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from 'src/app/Service/user.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {
  id:any;
  data: any;

  constructor(private route:ActivatedRoute, private userData:UserService, private router:Router) {
    this.id = this.route.snapshot.paramMap.get('id');
   }

  ngOnInit(): void {
     
    this.userData.getDataId(this.id).subscribe((result)=>{
      this.data = result
    })
  }
  getValues(value:any){
    console.log(value)
    this.userData.updateUser(this.id,value).subscribe((result)=>{
      console.log(result)
    })
    this.router.navigate(['/admin/profiles'])
  }
}
