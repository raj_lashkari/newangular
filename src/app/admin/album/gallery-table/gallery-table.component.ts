import { Component, OnInit } from '@angular/core';
import { GalleryService } from 'src/app/Service/gallery.service';

@Component({
  selector: 'app-gallery-table',
  templateUrl: './gallery-table.component.html',
  styleUrls: ['./gallery-table.component.css']
})
export class GalleryTableComponent implements OnInit {
  data: any;

  constructor(private gallerData:GalleryService) { }

  ngOnInit(): void {
    this.gallerData.showGalleryImage().subscribe((result)=>
    this.data = result
    )
  }

  deleteGallery(id:any){
    console.log(id)
    this.gallerData.deleteGallery(id).subscribe((result)=>{
      console.log(result)
      this.ngOnInit();
    })
  }

}
