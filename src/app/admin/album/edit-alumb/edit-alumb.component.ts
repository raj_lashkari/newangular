import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import { GalleryService } from 'src/app/Service/gallery.service';
import { Router } from '@angular/router'; 

@Component({
  selector: 'app-edit-alumb',
  templateUrl: './edit-alumb.component.html',
  styleUrls: ['./edit-alumb.component.css']
})
export class EditAlumbComponent implements OnInit {
  id:any;
  data: any;

  constructor(private album:GalleryService,private  route:ActivatedRoute , private router:Router) { 
    this.id = this.route.snapshot.paramMap.get('id');
    this.album.idAlbum(this.id).subscribe((result)=>{
      this.data = result
    })
    console.log(this.id)

  }

  ngOnInit(): void {
  }
  getValues(value:any){
    console.log(value)
    this.album.updateAlbum(this.id,value).subscribe((result)=>{
      console.log(result)
    })
    this.router.navigate(["/admin/albumTable"])
  }
   


}
