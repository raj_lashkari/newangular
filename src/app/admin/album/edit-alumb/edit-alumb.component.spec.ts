import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAlumbComponent } from './edit-alumb.component';

describe('EditAlumbComponent', () => {
  let component: EditAlumbComponent;
  let fixture: ComponentFixture<EditAlumbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditAlumbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAlumbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
