import { Component, OnInit } from '@angular/core';
import { GalleryService } from 'src/app/Service/gallery.service';
import { LoginService } from 'src/app/Service/login.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-album-form',
  templateUrl: './album-form.component.html',
  styleUrls: ['./album-form.component.css']
})
export class AlbumFormComponent implements OnInit {
  userId: any;

 constructor(private albumdData:GalleryService , private userData:LoginService , private router:Router){
   this.userData.login.subscribe((result)=>{
   
     this.userId = result.id
   })
 }
  ngOnInit(): void {

  }
  getValues(val:any){
     val.user_id =this.userId
      this.albumdData.album(val).subscribe((result)=>{
        console.log(result);
      })
      this.router.navigate(['admin'])
  }
  

}
