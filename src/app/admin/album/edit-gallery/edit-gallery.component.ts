import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GalleryService } from 'src/app/Service/gallery.service';
@Component({
  selector: 'app-edit-gallery',
  templateUrl: './edit-gallery.component.html',
  styleUrls: ['./edit-gallery.component.css']
})
export class EditGalleryComponent implements OnInit {
  id: any;
  data:any;
 files: any[]=[];
 imageurls:any =[];
 imageName:any;

  constructor(private route:ActivatedRoute, private gallery:GalleryService) {
    this.id = this.route.snapshot.paramMap.get('id');
    
   }
   onSelectFile(event:any) {
    if (event.target.files) {
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.imageurls = (<FileReader>event.target).result;
        };
        this.imageName= event.target.files[0].name;
             
    
    }
  }

  
   getValues(value:any){
  

      value.thumbnail = this.imageName
      this.gallery.updateGallery(this.id,value).subscribe((result)=>{
        console.log(result)
     })
  
   }

  ngOnInit(): void {
    this.gallery.idGallery(this.id).subscribe((result)=>{
      this.data = result
    })
    
  }



}
