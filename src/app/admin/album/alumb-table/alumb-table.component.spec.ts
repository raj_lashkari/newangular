import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AlumbTableComponent } from './alumb-table.component';

describe('AlumbTableComponent', () => {
  let component: AlumbTableComponent;
  let fixture: ComponentFixture<AlumbTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AlumbTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AlumbTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
