import { Component, OnInit } from '@angular/core';
import { GalleryService } from 'src/app/Service/gallery.service';
@Component({
  selector: 'app-alumb-table',
  templateUrl: './alumb-table.component.html',
  styleUrls: ['./alumb-table.component.css']
})
export class AlumbTableComponent implements OnInit {
  data: any;
  id: any;

  constructor(private album:GalleryService) { 
  

  }

  ngOnInit(): void {
    this.album.getAlbum().subscribe((result)=>
    this.data= result
    )
  }

  
  deleteData(id:any){
    console.log(id)
    this.album.deleteAlbum(id).subscribe((result)=>{
      this.ngOnInit();
    })
  }
}
