import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GalleryService } from 'src/app/Service/gallery.service';
import { LoginService } from 'src/app/Service/login.service';
@Component({
  selector: 'app-gallery-form',
  templateUrl: './gallery-form.component.html',
  styleUrls: ['./gallery-form.component.css']
})
export class GalleryFormComponent {
  imageurls:any =[];
  base64String: any = '';
  userId: any;
  data: any;
  arr:any[]=[];
  message: any;
constructor(private albumdData:GalleryService , private userData:LoginService, private router:Router){
  this.userData.login.subscribe((result)=>{
  
    this.userId = result.id
  })

  this.albumdData.getAlbum().subscribe((result)=>{

    this.data = result
  })


}



  onSelectFile(event:any) {
    if (event.target.files) {
      for (let i = 0; i < File.length + i; i++) {
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.imageurls = (<FileReader>event.target).result;
        };
        this.arr.push(event.target.files[i].name);
      }
    }
  }

  getValues(val:any){

    val.user_id =this.userId
    val.thumbnail = this.arr
   this.albumdData.galleryImagesData(val).subscribe((result)=>{this.message=result}); 
   this.router.navigate(['admin'])
   
 }



}
