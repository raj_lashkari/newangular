import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from 'src/app/Service/product.service';
@Component({
  selector: 'app-product-update',
  templateUrl: './product-update.component.html',
  styleUrls: ['./product-update.component.css']
})
export class ProductUpdateComponent implements OnInit {
  imageurls: any;
  imageName: any;
  id: any;
  data: any;

  constructor(private route:ActivatedRoute , private productData:ProductService) {
    this.id = this.route.snapshot.paramMap.get('id')
   }

  ngOnInit(): void {
    this.productData.getDataId(this.id).subscribe((result)=>{
      this.data = result
    })
  }
  onSelectFile(event:any) {
    if (event.target.files) {
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.imageurls = (<FileReader>event.target).result;
        };
        this.imageName= event.target.files[0].name;
             
    
    }
  }
  getValues(value:any){
    console.log(value)
    value.image = this.imageName;

  }
}
