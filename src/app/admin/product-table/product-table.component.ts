import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/Service/product.service';
@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {
  data: any;

  constructor(private productData:ProductService) {
    this.productData.getData().subscribe((result)=>{
      this.data = result
    })
   }

  ngOnInit(): void {
  }
  deleteData(id:any){

  }
  
}
