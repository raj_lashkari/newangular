import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/Service/product.service';
@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.css']
})
export class ProductAddComponent implements OnInit {
  imageurls:any;
  imageName: any;

  constructor(private productData:ProductService) { }

  ngOnInit(): void {
  }
  onSelectFile(event:any) {
    if (event.target.files) {
        var reader = new FileReader();
        reader.onload = (event: ProgressEvent) => {
          this.imageurls = (<FileReader>event.target).result;
        };
        this.imageName= event.target.files[0].name;
             
    
    }
  }
  getValues(value:any){
    console.log(value)
    value.image = this.imageName;
  this.productData.postData(value).subscribe((result)=>{
    console.log(result);
  })
  }
}
