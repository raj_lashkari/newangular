import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import {NgxImageCompressService} from 'ngx-image-compress';


import { AdminComponent } from './admin/admin.component';
import { GalleryFormComponent } from '../admin/album/gallery-form/gallery-form.component';
import { AlbumFormComponent } from '../admin/album/album-form/album-form.component';
import { AlumbTableComponent } from './album/alumb-table/alumb-table.component';
import { GalleryTableComponent } from './album/gallery-table/gallery-table.component';
import { EditAlumbComponent } from './album/edit-alumb/edit-alumb.component';
import { EditGalleryComponent } from './album/edit-gallery/edit-gallery.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UpdateUserComponent } from './user-profile/update-user/update-user.component';
import { BlogTableComponent } from './blog-table/blog-table.component';
import { BlogUpdateComponent } from './blog-table/blog-update/blog-update.component';
import { BlogAddComponent } from './blog-table/blog-add/blog-add.component';
import { ProductAddComponent } from './product-table/product-add/product-add.component';
import { ProductTableComponent } from './product-table/product-table.component';
import { ProductUpdateComponent } from './product-table/product-update/product-update.component';

const appRoutes:Routes = [
   
      {path:'',component:AdminComponent },
      {path:'galleryForm' , component : GalleryFormComponent}, 
      {path:'albumForm' , component : AlbumFormComponent},
      {path:'albumTable' , component : AlumbTableComponent},
      {path:'galleryTable' , component : GalleryTableComponent},
      {path:'editAlbum/:id' , component : EditAlumbComponent},
      {path:'editGallery/:id' , component : EditGalleryComponent},
      {path:'profiles' , component : UserProfileComponent},
      {path:'updateUsers/:id' , component : UpdateUserComponent},
      {path:'blogTable' , component : BlogTableComponent},
      {path:'blogupdate/:id' , component : BlogUpdateComponent},
      {path:'blogAdd' , component : BlogAddComponent},
      {path:'productAdd' , component : ProductAddComponent},
      {path:'productTable' , component : ProductTableComponent},
      {path:'productUpdate/:id' , component : ProductUpdateComponent},






      



]
   
@NgModule({
  declarations: [
    AdminComponent,
    GalleryFormComponent,
    AlbumFormComponent,
    AlumbTableComponent,
    GalleryTableComponent,
    EditAlumbComponent,
    EditGalleryComponent,
    UserProfileComponent,
    UpdateUserComponent,
    BlogTableComponent,
    BlogUpdateComponent,
    BlogAddComponent,
    ProductAddComponent,
    ProductTableComponent,
    ProductUpdateComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes),
    FormsModule,
    ReactiveFormsModule,

  ],
  providers: [NgxImageCompressService],
})
export class AdminModule {
  constructor(){
    console.log("admin module")
  }
 }
