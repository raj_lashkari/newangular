import { Component, OnInit } from '@angular/core';
import {ProductService} from '../../Service/product.service'
import {ActivatedRoute} from '@angular/router'


@Component({
  selector: 'app-productdetails',
  templateUrl: '/productdetails.component.html',
  styleUrls: ['/productdetails.component.css']
})
export class ProductdetailsComponent implements OnInit {

  data: any;
  id:any;
  constructor(private route:ActivatedRoute, private productData:ProductService) { 
    this.id = this.route.snapshot.paramMap.get('id');
    this.productData.getDataId(this.id).subscribe((result)=>{this.data = result})

  }

  ngOnInit(): void {
  }

}
