import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import {ProductService} from '../Service/product.service'


@Component({
  selector: 'app-productlist',
  templateUrl: '/productlist.component.html',
  styleUrls: ['/productlist.component.css']
})
export class ProductlistComponent implements OnInit {
  data: any;
   id:any;

  constructor(private route:ActivatedRoute, private productData:ProductService) { 
  
    this.productData.getData().subscribe((result)=>{this.data = result});
     
  }

  ngOnInit(): void {
  
  }


}
