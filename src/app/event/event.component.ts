import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking
import { EventService } from '../Service/event.service';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css']
})
export class EventComponent implements OnInit {
  event:any = '';
  getdata: any;
  constructor(private eventData: EventService) {    

  }
      
  
  calendarOptions: CalendarOptions = {
    initialView: 'dayGridMonth',
    weekends: true, // initial value
    dateClick: this.handleDateClick.bind(this), // bind is important!
    events:this.eventData.event
    
  };

  handleDateClick(arg:any) {
    this.eventData.event.forEach((element:any) => {
      if (element.date == arg.dateStr) {
        alert("Event Purpose:-"+
        element.description+"                                           "+
        "Event Start Date"+
        element.date+"                                                  "+
        "Event End Date"+
        element.endedat );
      }
  
    });
  }
   
   
  ngOnInit(): void {
  
    this.event = this.eventData.event
 
    }




}


