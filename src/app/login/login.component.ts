import { Component, OnInit } from '@angular/core';
import { FormControl , FormGroup,  Validators } from '@angular/forms';
import {LoginService} from '../Service/login.service'
import { Router } from '@angular/router'; 
import { User } from '../Service/user.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
 
 data:any []= [];
 userData:any []= [];
  user: any;
  loginData: any;
  role: any;



  constructor(private router:Router,private userdata: LoginService) { 
  
   }

  ngOnInit(): void {

  }


  loginForm = new FormGroup({
    email:new FormControl('',Validators.required),
    password:new FormControl('',Validators.required) 
})
get email(){return this.loginForm.get('email')}
get password(){return this.loginForm.get('password')}


loginUser(){
    console.log(this.loginForm.value)
     this.userdata.loginUser(this.loginForm.value).subscribe((result)=>{
       this.loginData = result
       this.role = this.loginData.user.role
  
       if(this.role == 0){
        this.router.navigate(['../'])
        }
        if(this.role == 1){
          this.router.navigate(['/admin'])
        }
    })
   
  }


}


