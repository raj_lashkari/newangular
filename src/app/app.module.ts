import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http'
import { AuthGuard } from './auth/auth.guard';
import {ScheduleModule,RecurrenceEditorModule } from '@syncfusion/ej2-angular-schedule';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CommonModule } from '@angular/common';
import { CarouselModule } from 'ngx-owl-carousel-o';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { NgxSortableModule } from 'ngx-sortable';


import { FullCalendarModule } from '@fullcalendar/angular'; // must go before plugins
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin!
import interactionPlugin from '@fullcalendar/interaction'; // a plugin!




import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { CategoryComponent } from './category/category.component';
import { InstagramComponent } from './instagram/instagram.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { HomeComponent } from './home/home.component';
import { ProductlistComponent } from './productlist/productlist.component';
import { ProductdetailsComponent } from './productlist/productdetails/productdetails.component';
import { BloglistComponent } from './bloglist/bloglist.component';
import { BlogdetailsComponent } from './bloglist/blogdetails/blogdetails.component';
import { NoPageComponent } from './no-page/no-page.component';
import { EventComponent } from './event/event.component';
import { EventRegistrationComponent } from './event/event-registration/event-registration.component';
import { CategoryProductComponent } from './category/category-product/category-product.component';
import { UserProfileComponent } from './user-profile/user-profile.component';
import { GalleryImagesComponent } from './album/gallery-images/gallery-images.component';
import { AlbumComponent } from './album/album.component';
import { UpdateUserComponent } from './user-profile/update-user/update-user.component';










const appRoutes:Routes = [
  {
    path:'admin' ,  loadChildren: () => import(`./admin/admin.module`).then(m => m.AdminModule)
  },

    {
      path:'' , component : HomeComponent
    },

    {
      path:'aboutUs' , component : AboutUsComponent
    },

    {
      path:'contactUs' , component : ContactUsComponent
    },

    {
      path:'ourblog' , component : BloglistComponent
    },


    {
      path:'registration' , component : RegistrationComponent
    },
    
    {
      path:'login' , component : LoginComponent
    },
    
  
    {path:'categoryProduct/:id',component:CategoryProductComponent},


    {path:'productlist/:id',component:ProductlistComponent},
    
      
    {path:'productdetails/:id' , component : ProductdetailsComponent },

    {
      path:'bloglist/:id' , component : BloglistComponent
    },
    
    {
      path:'blogdetails/:id' , component : BlogdetailsComponent
    },
    {
      path:'event' , children:[
        {path:'',component:EventComponent},
        {path:'eventRegistration' , component : EventRegistrationComponent
      },]
    },
    {
      path:'userProfile',canActivate: [AuthGuard]  , component : UserProfileComponent
    },
  
    {
      path:'album' , component : AlbumComponent
    },
    {
      path:'galleryimage/:id' , component : GalleryImagesComponent
    },
     
    {
      path:'userUpdate/:id' , component : UpdateUserComponent
    },

    {
      path:'**' , component : NoPageComponent
    },
   
   
    
   
]



FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin,
  
]);

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SliderComponent,
    FooterComponent,
    CategoryComponent,
    InstagramComponent,
    RegistrationComponent,
    LoginComponent,
    AboutUsComponent,
    ContactUsComponent,
    HomeComponent,
    ProductlistComponent,
    ProductdetailsComponent,
    BloglistComponent,
    BlogdetailsComponent,
    NoPageComponent,
    EventComponent,
    EventRegistrationComponent,
    CategoryProductComponent,
    UserProfileComponent,
    GalleryImagesComponent,
    AlbumComponent,
    UpdateUserComponent,  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    ScheduleModule,RecurrenceEditorModule,
    CalendarModule.forRoot({ provide: DateAdapter, useFactory: adapterFactory }) ,
    CommonModule,
    FullCalendarModule, // register FullCalendar with you app
    CarouselModule,
    BrowserAnimationsModule,
    NgxImageGalleryModule,
    DragDropModule,
    NgxSortableModule,
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
  
}
