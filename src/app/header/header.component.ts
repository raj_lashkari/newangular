import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'; 
import { LoginService } from '../Service/login.service';
import { User } from '../Service/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  displaydata:any ="";
   firstname:any;
   lastname:any;
   userData:any;
   user: any;
  dataaa: any;

  constructor(private router:Router ,private loginData:LoginService) { 
  
  }
  
  Logout() {

    this.loginData.getLogout()
    this.firstname="";
    this.lastname="";
 
  }
  
  ngOnInit(): void {
    this.userData = this.loginData.login.subscribe((result)=>{
      if(result){
        this.firstname = result.firstname
        this.lastname =result.lastname
      }
    })
  }


}
