import { Component, OnInit } from '@angular/core';
import {ProductCategoryService} from '../Service/product-category.service'
@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  data: any;

  constructor(private categoryData:ProductCategoryService) { 
    this.categoryData.getData().subscribe((result)=>{this.data = result})
  }

  ngOnInit(): void {
  }

}
