import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import {ProductCategoryService} from '../../Service/product-category.service'

@Component({
  selector: 'app-category-product',
  templateUrl: './category-product.component.html',
  styleUrls: ['./category-product.component.css']
})
export class CategoryProductComponent implements OnInit {
  id: any;
  dataCatid: any;

  constructor(private route:ActivatedRoute, private productCategoryData:ProductCategoryService) { 
  
    this.id = this.route.snapshot.paramMap.get('id');

     this.productCategoryData.getDatacatId(this.id).subscribe((result)=>{console.log(result),this.dataCatid = result});
  }

  ngOnInit(): void {
  }
}
