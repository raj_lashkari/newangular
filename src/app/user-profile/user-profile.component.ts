import { Component, OnInit } from '@angular/core';
import {LoginService} from '../Service/login.service';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  data: any;
  userData: any;
  id: any;

  constructor(private userProfile:LoginService) {
    this.userData = this.userProfile.login.subscribe((result)=>{
       this.id = result.id;})
   }

  ngOnInit(): void {
  this.userProfile.getUserProfile(this.id).subscribe((result)=>{
    this.data = result
  })
  }

}
