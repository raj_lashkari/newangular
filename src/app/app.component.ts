import { Component , OnInit} from '@angular/core';
import { LoginService } from './Service/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'TrainingProject';
  constructor(private loginUser:LoginService){}
  ngOnInit(){
    this.loginUser.autoLogin()
  }
  
}
