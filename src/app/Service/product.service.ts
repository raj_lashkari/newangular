import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = "http://127.0.0.1:8000/api/product/"

  constructor(private http:HttpClient) {
  
   }
  getData(){
    // console.log("service id"+id)
    return  this.http.get(this.url);
   
     
  }
  getDataId(id:any){
    // console.log("service id"+id)
    return  this.http.get(this.url+id);
   
     
  }

 postData(value:any){
   return this.http.post(this.url,value);
 }

}
