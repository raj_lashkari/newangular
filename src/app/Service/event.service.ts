import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class EventService {
 
  url = "http://127.0.0.1:8000/api/event"
  data: any;

  event:any='';

  
  constructor(private http:HttpClient) { 
     this.http.get(this.url).subscribe(data=>{
      this.event = data;})
   
  }
  



  postEventData(getData:any){
     
    this.event.push({
      title:getData.title,
      date:getData.createdat,
      endDate:getData.endedat,
      description:getData.description
      
      
      
    })
    
    return this.http.post(this.url,getData)
  }


   getEventData(){
    this.http.get(this.url).subscribe(data=>{
      this.event = data;})
   }


}


