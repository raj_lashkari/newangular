import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class GalleryService {
   url= "http://127.0.0.1:8000/api/album";
   imageUrl =  "http://127.0.0.1:8000/api/galleryimage";
  constructor(private http:HttpClient) {
   
   }

   album(value:any){
    return this.http.post( this.url,value)
   }

   updateAlbum(id:any,value:any){
    return this.http.put( "http://127.0.0.1:8000/api/album/"+id,value)

   }
   idAlbum(id:any){
    return this.http.get( "http://127.0.0.1:8000/api/album/"+id)

   }

   deleteAlbum(id:any){
    return this.http.delete( "http://127.0.0.1:8000/api/album/"+id)
   }

   getAlbum(){
    return this.http.get( "http://127.0.0.1:8000/api/album")
   }
     




   galleryImages(id:any){
    return this.http.get( "http://127.0.0.1:8000/api/galleryimage/"+id)

   }
    
   showGalleryImage(){
    return this.http.get( "http://127.0.0.1:8000/api/galleryimage")
   }
   galleryImagesData(value:any){
     console.log(value)
    return this.http.post(this.imageUrl,value)

   }
   idGallery(id:any){
    return this.http.get( "http://127.0.0.1:8000/api/galleryimage/"+id)

   }
   updateGallery(id:any,value:any){
    return this.http.put( "http://127.0.0.1:8000/api/galleryimage/"+id,value);

   }
   deleteGallery(id:any){
    return this.http.delete( "http://127.0.0.1:8000/api/galleryimage/"+id);

   }

   dragDrop(value:any){
    return this.http.post("http://127.0.0.1:8000/api/updateAlbum",value)
  }
}
