import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductCategoryService {

  url = "http://127.0.0.1:8000/api/productCategory"

  constructor(private http:HttpClient) { }

  getData(){
    return  this.http.get(this.url);
     
  }

  getDatacatId(id:any){
    return  this.http.get("http://127.0.0.1:8000/api/catProduct/"+id);
  }
}
