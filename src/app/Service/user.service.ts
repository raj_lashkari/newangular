import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getData() {
    return this.http.get('http://127.0.0.1:8000/api/user');
  }

  getDataId(id: any) {
    return this.http.get('http://127.0.0.1:8000/api/user/' + id);
  }

  updateUser(id: any, value: any) {
    return this.http.put('http://127.0.0.1:8000/api/user/' + id, value);
  }
  
  deleteUser(id:any){
    return this.http.delete('http://127.0.0.1:8000/api/user/' + id);

  }
  // registerUser(getData:any){
  //   return this.http.post(this.url,getData)

  // }
}
