import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BlogService {
  
 
   url = "http://127.0.0.1:8000/api/post/";

  constructor(private http:HttpClient) { }
  
  getData(){
    return  this.http.get(this.url);
     
  }
  getDataId(id:any){
    return  this.http.get(this.url+id);

  }

  addblog(value:any){
    return  this.http.post(this.url,value);

  }

  blodUpdate(id:any,value:any){
    return  this.http.put( this.url+id,value);

  }
  blodDelete(id:any){
    return  this.http.delete( this.url+id);

  }
}
