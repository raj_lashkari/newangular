import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user.model';
import { BehaviorSubject, Subject } from 'rxjs';
import{tap} from 'rxjs/operators'
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class LoginService {
  
  nullAble:any= null
  public login = new BehaviorSubject<User>(this.nullAble); 
  
  url = "http://127.0.0.1:8000/api/login"
  logindata:any;
  userData: any;
  localStore: any;

  constructor(private http:HttpClient, private router:Router) { 

  }

  loginUser(getData:any){

    return this.http.post(this.url,getData).pipe(tap((result)=>{
          this.logindata=result;
          const userData = new User(
                this.logindata.user.id,
                this.logindata.user.firstname,
                this.logindata.user.lastname,
                this.logindata.user.email,
                this.logindata.user.gender,
                this.logindata.user.role,
                this.logindata.token
            );   
   
          this.login.next(userData)
      
          localStorage.setItem('userData',JSON.stringify(userData));
     })) 
  }

  getData(user:any){
   console.log("user"+user)
  }

 getLogout(){
   const userlogout = new User(null,null,null,null,null,null,null)
   this.login.next(userlogout)
   localStorage.clear();
   this.router.navigate(['/login']);
 }



 getUserProfile(id:any){
   return this.http.get("http://127.0.0.1:8000/api/user/"+id)
 }
 
 autoLogin(){
   this.localStore =  localStorage.getItem('userData')
  const userLoadedData:{id:any;firstname:any; lastname:any; email:any; gender:any;role:any;token:any } = JSON.parse( this.localStore );
    if(!userLoadedData){
      return;
    }

    const loadedUser = new User(
      userLoadedData.id,
      userLoadedData.firstname,
      userLoadedData.lastname,
      userLoadedData.email,
      userLoadedData.gender,
      userLoadedData.role,
      userLoadedData.token
      );

      if(loadedUser.token){
        this.login.next(loadedUser);
      }

    }

}


