import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../Service/login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  userData: any;
  token: any;
  constructor(private router : Router , private loginUser:LoginService){
    this.userData = this.loginUser.login.subscribe((result)=>{
      
      if(result){
        this.token = result.token
      }
    })
   
  }
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

      if (this.token)
      return true;
      this.router.navigate(['/login']);
      return false;
  
  }
  
}
