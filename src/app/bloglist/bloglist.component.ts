import { Component, OnInit } from '@angular/core';
import {BlogService} from '../Service/blog.service'


@Component({
  selector: 'app-bloglist',
  templateUrl: './bloglist.component.html',
  styleUrls: ['./bloglist.component.css']
})
export class BloglistComponent implements OnInit {

  data:any;


  
  constructor(private blogdata: BlogService) {
    
    this.blogdata.getData().subscribe((blogdata) => this.data = blogdata);
   }

  ngOnInit(): void {
  }

}
