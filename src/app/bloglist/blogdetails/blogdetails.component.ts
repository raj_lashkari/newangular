import { Component} from '@angular/core';
import {BlogService} from '../../Service/blog.service'
import {ActivatedRoute} from '@angular/router'

@Component({
  selector: 'app-blogdetails',
  templateUrl: './blogdetails.component.html',
  styleUrls: ['./blogdetails.component.css']
})
export class BlogdetailsComponent  {

 data:any;
  id: any;
  constructor(private route:ActivatedRoute,private blogdata: BlogService) { 
    this.id = this.route.snapshot.paramMap.get('id');

    this.blogdata.getDataId(this.id).subscribe((blogdata) => this.data = blogdata);
   }


}
